# ScreenTime #

ScreenTime uses the [Affdex SDK for OS X](http://developer.affectiva.com/v3_1_1/osx/) to measure how long you are in front of your computer.

## TODO ###

* determine if we should use Core data to store data.  We need to store data points in a location that can be used to make a graph
* after x minutes at your screen show popup warning
* add to menubar in OSX
* figure out how to use less CPU cycles

## Developer Usage ##

1. Install cocoapods.  This is a ruby gem used to download and install the Affdex SDK

    sudo gem install cocoapods

2. Install dependencies from Podfile.  This will fetch the Affdex SDK pod and install it into the project

    cd ScreenTime
    pod install