//
//  main.m
//  ScreenTime
//
//  Created by Jay Prall on 9/22/16.
//  Copyright © 2016 affectiva. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
