//
//  ViewController.h
//  ScreenTime
//
//  Created by Jay Prall on 9/22/16.
//  Copyright © 2016 affectiva. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Affdex/Affdex.h>

@interface ViewController : NSViewController <AFDXDetectorDelegate>

@property (strong) AFDXDetector *detector;
@property (nonatomic, retain) NSTimer *appTimer;
@property (nonatomic, assign) int timerCount;
@property (nonatomic, assign) IBOutlet NSTextField *stopWatchLabelTextField;
@property (nonatomic, strong) IBOutlet NSImageView *screenStatusImage;

@end

