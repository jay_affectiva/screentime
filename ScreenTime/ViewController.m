//
//  ViewController.m
//  ScreenTime
//
//  Created by Jay Prall on 9/22/16.
//  Copyright © 2016 affectiva. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController


-(void)viewDidAppear {
    [super viewDidAppear];

    // set window title
    self.view.window.title = @"ScreenTime";
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.stopWatchLabelTextField.editable = FALSE;
    
    // reset the watch
    [self resetStopWatch:nil];

    // use the layer property of NS View
    self.view.wantsLayer = true;

    // create our detector with our desired facial expresions, using the front facing camera
    self.detector = [[AFDXDetector alloc] initWithDelegate:self
                                               usingCamera:AFDX_CAMERA_FRONT maximumFaces:1];
    // tell the detector which facial expressions we want to measure
    self.detector.smile = TRUE;

    // lower frame rate to use less CPU
    self.detector.maxProcessRate = 5.0;
    
    // start the camera
    NSError *error = [self.detector start];
    
    //turn background red, to indicate no face found yet
    self.view.layer.backgroundColor = [NSColor redColor].CGColor;

    // display an error if we couldn't start the camera
    if (error != nil)
    {
        NSAlert *alert = [NSAlert new];
        alert.messageText = [error localizedDescription];
        [alert runModal];
    }
}

-(NSString *)getFormattedString
{
    int t = self.timerCount;
    int minQuotient = t / 600;
    int minRemainder = t % 600;
    int secQuotient = minRemainder / 10;
    int secRemainder = minRemainder % 10;
    
    NSString * minString = [NSString stringWithFormat:@"%d",minQuotient];
    NSString * secString = [NSString stringWithFormat:@"%d",secQuotient];
    
    if (secQuotient < 10){
        secString = [NSString stringWithFormat:@"0%@",secString];
    }
    NSString *secRemainderString = [NSString stringWithFormat:@"%d",secRemainder];
    
    return [NSString stringWithFormat:@"%@:%@.%@",minString,secString,secRemainderString];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

-(IBAction)startStopWatch:(id)sender
{
    [self.appTimer invalidate];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                      target:self selector:@selector(stopWatch:)
                                                    userInfo:nil repeats:YES];
    self.appTimer = timer;
}

-(IBAction)stopStopWatch:(id)sender
{
    [self.appTimer invalidate];
}

-(IBAction)resetStopWatch:(id)sender
{
    [self.appTimer invalidate];
    self.timerCount = 0;
    [self.stopWatchLabelTextField setStringValue:[self getFormattedString]];
}

- (void)stopWatch:(NSTimer*)theTimer {
    self.timerCount = self.timerCount + 1;
    [self.stopWatchLabelTextField setStringValue:[self getFormattedString]];
    
}

- (void)detector:(AFDXDetector *)detector didStartDetectingFace:(AFDXFace *)face;
{
    // turn background color green
    self.view.layer.backgroundColor = [NSColor greenColor].CGColor;

    self.screenStatusImage.image = [NSImage imageNamed:@"found"];

    
    // start the watch
    [self startStopWatch:nil];
}

- (void)detector:(AFDXDetector *)detector didStopDetectingFace:(AFDXFace *)face;
{
    // turn background color red
    self.view.layer.backgroundColor = [NSColor redColor].CGColor;
    
    self.screenStatusImage.image = [NSImage imageNamed:@"lost"];

    
    // stop the watch
    [self stopStopWatch:nil];

}

- (void)detector:(AFDXDetector *)detector hasResults:(NSMutableDictionary *)faces forImage:(NSImage *)image atTime:(NSTimeInterval)time
{
    // called when the detector has processed a video frame from the camera
    //    NSLog(@"faces = %@", faces);
}


@end
